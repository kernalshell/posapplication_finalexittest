package com.nagarro.pos.constants;

import java.lang.annotation.Documented;

/**
 * interface annotation for Javadoc
 */
@MyDoc(author = Constants.AUTHOR, date = Constants.CREATION_DATE, currentRevision = 1)
@Documented
public @interface MyDoc {
	String author();

	String date();

	int currentRevision() default 1;

}
