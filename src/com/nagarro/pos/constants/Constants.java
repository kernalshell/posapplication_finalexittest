package com.nagarro.pos.constants;

/**
 * constants file
 */
@MyDoc(author = Constants.AUTHOR, date = Constants.CREATION_DATE, currentRevision = 1)
public class Constants {
	private Constants() {
	}

	public static final String MD5 = "MD5";
	public static final String USER = "employee";
	public static final String MSG = "msg";
	public static final String REDIRECT_DEFUALT = "redirect:/";
	public static final String EMPLOYEE = "EMPLOYEE";
	public static final String ADMIN = "ADMIN";
	public static final String ASC = "asc";

	public static final String CREATION_DATE = "27/03/2018";
	public static final String AUTHOR = "Manhar Gupta";
	public static final char[] CHARSET_AZ_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();

}
