package com.nagarro.pos.services.employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.pos.dao.employee.EmployeeDao;
import com.nagarro.pos.exceptions.CustomException;
import com.nagarro.pos.models.Employee;
import com.nagarro.pos.models.UserSecret;

@Service
@Transactional
public class EmployeeService {

	@Autowired
	EmployeeDao employeeDao;

	@Transactional
	public Employee verifyEmployee(String email, String password) throws CustomException {
		Employee emp = null;
		try {
			final Employee currEmp = employeeDao.getEmployeeByEmail(email);
			final UserSecret userSecret = employeeDao.getPass(currEmp.getId());
			if (currEmp != null && userSecret != null) {
				if (userSecret.getPass().equals(password)) {
					emp = currEmp;
				}
			}
		} catch (final CustomException e) {
			throw new CustomException(e.getMessage());
		}

		return emp;
	}

}
