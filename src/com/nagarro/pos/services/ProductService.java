package com.nagarro.pos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.pos.dao.ProductDao;
import com.nagarro.pos.models.Products;

@Service
@Transactional
public class ProductService {

	@Autowired
	ProductDao dao;

	public Products getProductById(int pid) {
		return dao.getProductById(pid);
	}
}
