package com.nagarro.pos.services;

import java.io.Console;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.pos.controllers.EmployeeController;
import com.nagarro.pos.dao.CartDaoImpl;
import com.nagarro.pos.dao.CustomerDao;
import com.nagarro.pos.exceptions.CustomException;
import com.nagarro.pos.models.Cart;
import com.nagarro.pos.models.Customer;
import com.nagarro.pos.models.Products;

@Service
public class CartService {

	final Logger logger = Logger.getLogger(CartService.class);

	@Autowired
	CartDaoImpl dao;

	@Autowired
	ProductService pdao;

	@Autowired
	CustomerDao custDao;

	@Transactional(rollbackFor = Exception.class)
	public Products addProductToCart(int pid, int custId) throws Exception {

		long ret = -1;
		Products product = null;
		try {
			product = pdao.getProductById(pid);
			Customer customer = custDao.getCustomerById(custId);
			Cart newCartData = new Cart();
			newCartData.setQuantity(1);
			newCartData.setUser(customer);
			newCartData.getProducts().add(product);
			product.getCart().add(newCartData);
			customer.getCart().add(newCartData);

			newCartData.setCreated(new Date());
			newCartData.setUpdated(new Date());
			
			dao.addProductToCart(newCartData);
			/*
			 * if (ret < 1) throw new CustomException("Unable to ad product To cart");
			 */

		} catch (Exception e) {
			logger.error(e);
			throw new Exception(e.getMessage());
		}
		return product;
	}
}
