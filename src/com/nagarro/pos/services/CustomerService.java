package com.nagarro.pos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.pos.dao.CustomerDao;
import com.nagarro.pos.models.Customer;

@Service
@Transactional
public class CustomerService {

	@Autowired
	CustomerDao dao;

	@Transactional(readOnly = true)
	public Customer getCustomerById(int custId) {
		return dao.getCustomerById(custId);
	}
}
