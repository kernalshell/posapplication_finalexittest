package com.nagarro.pos.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nagarro.pos.dto.ErrorMessage;
import com.nagarro.pos.models.Products;
import com.nagarro.pos.services.CartService;

@Controller
@RequestMapping("/carts")
public class CartController {

	@Autowired
	CartService cartSevice;

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Object> getOrder(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @RequestParam("pid") int pid, @RequestParam("custId") int custId) {
		Products product = null;
		try {

			product = cartSevice.addProductToCart(pid, custId);
			System.out.println(product);
		} catch (Exception e) {
			e.printStackTrace();
			final ErrorMessage em = new ErrorMessage();
			em.setErrorMessage("Some Error Occured");
			em.setFlag(false);
			return ResponseEntity.badRequest().body(em);
		}

		return ResponseEntity.ok().body(product);

	}

}
