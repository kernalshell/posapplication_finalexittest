package com.nagarro.pos.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nagarro.pos.constants.Constants;
import com.nagarro.pos.dto.ErrorMessage;
import com.nagarro.pos.exceptions.CustomException;
import com.nagarro.pos.models.Employee;
import com.nagarro.pos.services.employee.EmployeeService;
import com.nagarro.pos.validators.Validator;

@Controller
@RequestMapping("/employees")
public class EmployeeController {

	final Logger logger = Logger.getLogger(EmployeeController.class);

	@Autowired
	EmployeeService employeeService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	public ResponseEntity<Object> getOrder(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @ModelAttribute("employee") Employee employee) {

		try {
			System.out.println(employee.getEmail());
			System.out.println(employee.getUserSecret().getPass());
			Validator.validateField(employee.getEmail());
			Validator.validateField(employee.getUserSecret().getPass());

		} catch (final CustomException e2) {
			logger.error(e2);
			final ErrorMessage em = new ErrorMessage();
			em.setErrorMessage("Some Error Occured");
			em.setFlag(false);
			return ResponseEntity.ok().body(em);
		}

		Employee emp;
		try {
			emp = employeeService.verifyEmployee(employee.getEmail(), employee.getUserSecret().getPass());
			if (emp != null) {
				session.setAttribute(Constants.USER, emp);
				return ResponseEntity.ok().body(emp);
			}
		} catch (CustomException e) {
			e.printStackTrace();
		}

		final ErrorMessage em = new ErrorMessage();
		em.setErrorMessage("Some Error Occured");
		em.setFlag(false);
		return ResponseEntity.ok().body(em);

	}

}
