package com.nagarro.pos.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.nagarro.pos.constants.Constants;
import com.nagarro.pos.constants.MyDoc;
import com.nagarro.pos.constants.OrderStatus;
import com.nagarro.pos.constants.PaymentType;
import com.nagarro.pos.dto.ErrorMessage;
import com.nagarro.pos.models.Orders;
import com.nagarro.pos.services.OrdersService;

@MyDoc(author = Constants.AUTHOR, date = Constants.CREATION_DATE, currentRevision = 1)
@Controller
@RequestMapping(value = "/orders")
public class OrdersController {

	final Logger logger = Logger.getLogger(OrdersController.class);

	@Autowired
	OrdersService ordersService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getOrder(HttpServletRequest request, HttpServletResponse response,
			HttpSession session) {
		final ErrorMessage em = new ErrorMessage();
		em.setErrorMessage("Please login first");
		em.setFlag(false);
		return ResponseEntity.ok().body(em);

	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> postOrder(HttpServletRequest request, HttpServletResponse response,
			HttpSession session, @RequestParam("paymode") String paymode, @RequestParam("status") String status,
			@RequestParam("custId") String custId, @RequestParam("empId") String empId) {
		final Orders orders = ordersService.saveOrder(PaymentType.valueOf(paymode.toUpperCase()),
				OrderStatus.valueOf(status.toUpperCase()), custId, empId);
		final ErrorMessage em = new ErrorMessage();
		em.setErrorMessage("Please login first");
		em.setFlag(false);
		return ResponseEntity.ok().body(em);

	}

}
