package com.nagarro.pos.exceptions;

import com.nagarro.pos.constants.Constants;
import com.nagarro.pos.constants.MyDoc;

@MyDoc(author = Constants.AUTHOR, date = Constants.CREATION_DATE, currentRevision = 1)
public class CustomException extends Exception {

	private static final long serialVersionUID = 192003551293800393L;

	public CustomException(String s) {
		super(s);
	}
}
