package com.nagarro.pos.dao.employee;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nagarro.pos.exceptions.CustomException;
import com.nagarro.pos.models.Employee;
import com.nagarro.pos.models.UserSecret;

@Repository
public class EmployeeDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public Employee getEmployeeByEmail(String email) throws CustomException {
		try {
			Employee emp = null;
			final Query query = getCurrentSession().createQuery("from Employee where email=:email");
			query.setParameter("email", email);
			emp = (Employee) query.getSingleResult();
			return emp;
		} catch (Exception ex) {
			throw new CustomException(ex.getMessage());
		}
	}

	public UserSecret getPass(int id) throws CustomException {
		UserSecret userSecret = null;
		final Session session = sessionFactory.getCurrentSession();
		try {
			final Query query = session.createQuery("from UserSecret where id=:id");

			query.setParameter("id", id);
			userSecret = (UserSecret) query.getSingleResult();
		} catch (final Exception e) {
			throw new CustomException("Error: Getting UserSecret");
		}
		return userSecret;
	}
}
