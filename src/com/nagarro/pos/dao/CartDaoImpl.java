package com.nagarro.pos.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nagarro.pos.models.Cart;

@Repository
public class CartDaoImpl {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public boolean addProductToCart(Cart cart) {
		boolean flag = false;
		try {
			getCurrentSession().save(cart);
			flag = true;
		} catch (Exception e) {
			flag = false;
		}

		return flag;

	}

}