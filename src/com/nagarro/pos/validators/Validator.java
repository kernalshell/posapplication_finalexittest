package com.nagarro.pos.validators;

import java.util.Properties;

import com.nagarro.pos.constants.Constants;
import com.nagarro.pos.constants.MyDoc;
import com.nagarro.pos.exceptions.CustomException;
import com.nagarro.pos.utilities.UserProperties;

/*
 * Validator class used for validating the input.
 */
@MyDoc(author = Constants.AUTHOR, date = Constants.CREATION_DATE, currentRevision = 1)
public class Validator {
	static Properties prop = UserProperties.getProperties();

	private Validator() {
	}

	/* validate field entered by user */
	public static void validateField(String field) throws CustomException {
		if (field == null || field.isEmpty()) {
			throw new CustomException(prop.getProperty("EXCEP_FIELD_EMPTY"));
		}
	}
}
