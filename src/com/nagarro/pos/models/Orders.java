package com.nagarro.pos.models;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.nagarro.pos.constants.Constants;
import com.nagarro.pos.constants.MyDoc;
import com.nagarro.pos.constants.PaymentType;

@MyDoc(author = Constants.AUTHOR, date = Constants.CREATION_DATE, currentRevision = 1)
@Entity
public class Orders extends AbstractTimestampEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1530339338969797035L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@GenericGenerator(name = "order_id", strategy = "com.nagarro.pos.hibernate.TIckerIdGenerator")
	@GeneratedValue(generator = "order_id")
	private String orderId;
	private String status;
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderDate;
	@Enumerated(EnumType.STRING)
	private PaymentType paymentType;

	@JoinTable(name = "orders_products", joinColumns = {
			@JoinColumn(name = "orderId", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "productId", nullable = false, updatable = false) })
	@ManyToMany(cascade = CascadeType.ALL)
	private Set<Products> products = new HashSet<>();

	@ManyToOne(cascade = CascadeType.ALL)
	private Employee employee;

	@ManyToOne(cascade = CascadeType.ALL)
	private Customer customer;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public Set<Products> getProducts() {
		return products;
	}

	public void setProducts(Set<Products> products) {
		this.products = products;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}
