package com.nagarro.pos.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nagarro.pos.constants.Constants;
import com.nagarro.pos.constants.MyDoc;

@MyDoc(author = Constants.AUTHOR, date = Constants.CREATION_DATE, currentRevision = 1)
@Entity
public class Products extends AbstractTimestampEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5094159898920442975L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String pcode;
	private String name;
	private int stock;
	private String description;
	
	@JsonIgnore
	@JoinTable(name = "products_carts", joinColumns = {
			@JoinColumn(name = "productId", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "cartId", nullable = false, updatable = false) })
	@ManyToMany(cascade = CascadeType.ALL)
	private Set<Cart> cart = new HashSet<>();

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "products")
	private Set<Orders> order = new HashSet<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPcode() {
		return pcode;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Cart> getCart() {
		return cart;
	}

	public void setCart(Set<Cart> cart) {
		this.cart = cart;
	}

	public Set<Orders> getOrder() {
		return order;
	}

	public void setOrder(Set<Orders> order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "Products [id=" + id + ", pcode=" + pcode + ", name=" + name + ", stock=" + stock + ", description="
				+ description + "]";
	}

}
